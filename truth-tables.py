#!/usr/bin/env python3

"""
Pandoc filter using panflute
Take code blocks with the class 'ttable' and return a truth table as a Pandoc table

Example input:

```{.ttable}
P and Q
```

"""

import re
import panflute as pf
import ttg


def captionify(text):
    """
    Take a string and return a Caption object, with the string converted to Pandoc objects within it.
    """
    return pf.Caption(pf.Plain(*pf.convert_text(text.strip())[0].content))


def latexify(text):
    """
    Take a string and return a LaTeX version of the formula.

    negation: 'not', '-', '~'
    logical disjunction: 'or'
    logical nor: 'nor'
    exclusive disjunction: 'xor', '!='
    logical conjunction: 'and'
    logical NAND: 'nand'
    material implication: '=>', 'implies'
    logical biconditional: '='
    """
    # negation
    text = re.sub("not\\s", "\\\\lnot ", text)
    text = re.sub("-", "\\\\lnot ", text)
    text = re.sub("~", "\\\\lnot ", text)
    # logical disjunction
    text = re.sub("\\sor\\s", " \\\\lor ", text)
    # logical nor
    text = re.sub("\\snor\\s", " \\\\downarrow ", text)
    # exclusive disjunction
    text = re.sub("\\sxor\\s", " \\\\oplus ", text)
    text = re.sub("\\s!=\\s", " \\\\oplus ", text)
    # logical conjunction
    text = re.sub("\\sand\\s", " \\\\land ", text)
    # logical NAND
    text = re.sub("\\snand\\s", " \\\\uparrow ", text)
    # material implication
    text = re.sub("\\s=>\\s", " \\\\rightarrow ", text)
    text = re.sub("\\simplies\\s", " \\\\rightarrow ", text)
    # logical biconditional
    text = re.sub("\\s=\\s", " \\\\leftrightarrow ", text)
    return text


def math_cells(elem, doc):
    """
    Take a TableCell and return a TableCell where the content of that cell is in an inline Math object.
    """
    if isinstance(elem, pf.TableCell):
        return pf.TableCell(
            pf.Plain(pf.Math(latexify(pf.stringify(elem)), "InlineMath")),
        )
    return None


def ttables(elem, doc):
    """
    Turn fenced code blocks into table objects if they have the .ttable class
    """
    # Run only for code blocks with the .ttable class
    if isinstance(elem, pf.CodeBlock) and "ttable" in elem.classes:
        # The formulae, as a list
        formulae = elem.text.split(",")
        # I need to get the letters in the formulae, for the generator
        # This gets everything upper case, and puts them in order
        letters = sorted(set(re.findall("[A-Z]", elem.text)))
        # make the table with the truth-table-generator library
        truth_table = ttg.Truths(
            letters,
            formulae,
        )
        # make the markdown table
        markdown_table = truth_table.as_tabulate(
            index=False,  # no line numbers
            table_format="simple",  # this is a format that Pandoc can read
        )
        pandoc_table = pf.convert_text(markdown_table)[0]  # convert_text returns a list
        # Use a caption attribute as the table caption
        if "caption" in elem.attributes:
            pandoc_table.caption = captionify(elem.attributes["caption"])
            elem.attributes.pop("caption")
        # Pass through identifier, classes, and attributes
        pandoc_table.identifier = elem.identifier
        pandoc_table.classes = elem.classes
        pandoc_table.attributes = elem.attributes
        # Turn the contents of the table into maths
        pandoc_table = pandoc_table.walk(math_cells)
        return pandoc_table
    return None


def main(doc=None):
    return pf.run_filter(ttables, doc=doc)


if __name__ == "__main__":
    main()