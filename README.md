# truth-tables.py

A [Pandoc](https://pandoc.org/) [filter](https://pandoc.org/filters.html) to generate [truth tables](https://en.wikipedia.org/wiki/Truth_table) from [propositional calculus](https://en.wikipedia.org/wiki/Propositional_calculus) formulae.

Example usage:

`$ pandoc --filter=truth-tables.py --to=beamer --output=slides.pdf slides.md`

The filter will convert code blocks with the 'ttable' attribute to truth tables, as long as the contents are understandable by the Python truth table generator module.

    ```{.ttable}
    P implies Q
    ```

The formulae are converted into [LaTeX](https://www.latex-project.org/), and treated as maths by Pandoc.

* negation: 'not', '-', '~' to `\lnot`
* logical disjunction: 'or' to `\lor`
* logical nor: 'nor' to `\downarrow`
* exclusive disjunction: 'xor', '!=' to `\oplus`
* logical conjunction: 'and' to `\land`
* logical NAND: 'nand' to `\uparrow`
* material implication: '=>', 'implies' to `\rightarrow`
* logical biconditional: '=' to `\leftrightarrow`

A caption for the table can be set with the 'caption' attribute, e.g., `caption='The Caption'`.

Only uppercase Roman letters ('P', 'Q', 'R', etc.) can be used for sentences.

Uses [panflute](https://pypi.org/project/panflute/) and [truth-table-generator](https://pypi.org/project/truth-table-generator/).

Tested with truth-table-generator 1.1.2, panflute 2.3.0, and pandoc 2.17.1.1.
