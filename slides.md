---
title: Truth tables
author: Thomas Hodgson
date: 27 February 2023
---

# A truth table

```{.ttable}
P = Q
```

# Another truth table

```{.ttable}
P implies Q, Q implies R, P implies R
```

# With a caption

```{.ttable caption='De Morgan'}
not (P or Q), (not P) and (not Q)
```